const express = require('express');
const router = express.Router();
const Commande = require('../../model/Commande');
const  CommandeNotFoundException = require('../../exceptions/CommandeNotFoundException');
const  ImpossibleAjouterCommandeException = require('../../exceptions/ImpossibleAjouterCommandeException')

// Create a new commande
router.post('/', async (req, res) => {
  try {
    const commandeData = req.body;

    const nouvelleCommande = new Commande(commandeData);
    const savedCommande = await nouvelleCommande.save();

    if (!savedCommande) {
      throw new ImpossibleAjouterCommandeException("Impossible d'ajouter cette commande");
    }

    res.status(201).json(savedCommande);
  } catch (error) {
    console.error('Error adding commande:', error);
    if (error instanceof ImpossibleAjouterCommandeException) {
      res.status(400).json({ error: error.message });
    } else {
      res.status(500).json({ error: 'An error occurred' });
    }
  }
});

// Get a single commande by ID
router.get('/:id', async (req, res) => {
  try {
    const commandeId = req.params.id;
    const commande = await Commande.findById(commandeId);

    if (!commande) {
      throw new CommandeNotFoundException("Cette commande n'existe pas");
    }

    res.json(commande);
  } catch (error) {
    console.error('Error retrieving commande:', error);
    if (error instanceof CommandeNotFoundException) {
      res.status(404).json({ error: error.message });
    } else {
      res.status(500).json({ error: 'An error occurred' });
    }
  }
});

// Update a commande
router.put('/', async (req, res) => {
  try {
    const commandeData = req.body;
    const updatedCommande = await Commande.findByIdAndUpdate(commandeData._id, commandeData, { new: true });

    if (!updatedCommande) {
      throw new CommandeNotFoundException("Cette commande n'existe pas");
    }

    res.json(updatedCommande);
  } catch (error) {
    console.error('Error updating commande:', error);
    if (error instanceof CommandeNotFoundException) {
      res.status(404).json({ error: error.message });
    } else {
      res.status(500).json({ error: 'An error occurred' });
    }
  }
});

module.exports = router;
