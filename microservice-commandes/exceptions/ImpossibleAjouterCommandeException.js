class ImpossibleAjouterCommandeException extends Error {
    constructor(message) {
      super(message);
      this.name = 'ImpossibleAjouterCommandeException';
      this.statusCode = 500;
    }
  }
  
  const errorHandler = (err, req, res, next) => {
    if (err.name === 'ImpossibleAjouterCommandeException') {
      res.status(err.statusCode).json({ error: err.message });
    } else {
      // Handle other errors or pass to the default error handler
      next(err);
    }
  };
  
module.exports = { ImpossibleAjouterCommandeException, errorHandler };
  