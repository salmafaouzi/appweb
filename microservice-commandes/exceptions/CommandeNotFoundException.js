class CommandeNotFoundException extends Error {
    constructor(message) {
      super(message);
      this.name = 'CommandeNotFoundException';
      this.statusCode = 404;
    }
  }
  
  const errorHandler = (err, req, res, next) => {
    if (err.name === 'CommandeNotFoundException') {
      res.status(err.statusCode).json({ error: err.message });
    } else {
      // Handle other errors or pass to the default error handler
      next(err);
    }
  };
  
module.exports = { CommandeNotFoundException, errorHandler };
  