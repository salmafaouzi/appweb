const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
require('dotenv').config();

const app = express();

app.use(bodyParser.json());

// Connect to MongoDB
mongoose
  .connect(process.env.MONGODB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log('MongoDB connected ...'))
  .catch((err) => console.log(err));

// Import routes
const commandesRouter = require('./routes/api/commandes');

// Configure routes
app.use('/api/commandes', commandesRouter);

const commandPort = 4000;

app.listen(commandPort, () => {
  console.log(`Command microservice running on port ${commandPort}`);
});
