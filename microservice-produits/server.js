const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
require('dotenv').config();
require('dotenv').config({ path: '/run/secrets/microservice-produits-env' });


const app = express();

app.use(bodyParser.json());

// Connect to MongoDB
mongoose
  .connect(process.env.MONGODB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log('MongoDB connected ...'))
  .catch((err) => console.log(err));

// Serve static files from the "images" folder
app.use('/images', express.static(__dirname + '/images'));

// Import routes
const produitsRouter = require('./routes/api/produits');

// Configure routes
app.use('/api/produits', produitsRouter);

const port = 8080;
app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
