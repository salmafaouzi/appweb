const mongoose = require('mongoose');

const produitSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  image: {
    type: String,
    required: true,
  },
  price: {
    type: String,
    required: true,
  },
});

const Produit = mongoose.model('Produit', produitSchema);

module.exports = Produit;
