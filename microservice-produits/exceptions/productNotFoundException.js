// productNotFoundException.js

class ProductNotFoundException extends Error {
    constructor(message) {
      super(message);
      this.name = 'ProductNotFoundException';
      this.status = 404;
    }
  }
  
module.exports = ProductNotFoundException;
  