const express = require('express');
const app = express();

const Produit = require('../../model/Produit');
const ProductNotFoundException = require('./productNotFoundException');
const errorMiddleware = require('./errorMiddleware');

app.get('/produits/:id', (req, res, next) => {
  const produitId = req.params.id;

  Produit.findById(produitId)
    .then((produit) => {
      if (!produit) {
        throw new ProductNotFoundException('Produit not found');
      }

      res.json(produit);
    })
    .catch((error) => {
      console.error('Error fetching produit:', error);
      res.status(500).json({ error: 'Failed to fetch produit' });
    });
});

app.use(errorMiddleware);

app.listen(3000, () => {
  console.log('Server is running on port 3000');
});
