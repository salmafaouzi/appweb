// errorMiddleware.js

function errorMiddleware(err, req, res, next) {
    if (err.name === 'ProductNotFoundException') {
      res.status(err.status).json({ error: err.message });
    } else {
      // For other errors, you can provide a generic error response
      res.status(500).json({ error: 'Internal Server Error' });
    }
  }
  
module.exports = errorMiddleware;
  