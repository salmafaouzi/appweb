const express = require('express');
const router = express.Router();

const Produit = require('../../model/Produit');

// Route pour récupérer tous les produits
router.get('/', (req, res) => {
  Produit.find()
    .then((produits) => {
      res.json(produits);
    })
    .catch((error) => {
      console.error('Error fetching produits:', error);
      res.status(500).json({ error: 'Failed to fetch produits' });
    });
});

// Route pour créer un nouveau produit
router.post('/', (req, res) => {
  const { name, description, image, price } = req.body;
  const newProduit = new Produit({ name, description, image, price });

  newProduit.save()
    .then((savedProduit) => {
      console.log('Produit saved:', savedProduit);
      res.status(201).json(savedProduit);
    })
    .catch((error) => {
      console.error('Error saving produit:', error);
      res.status(500).json({ error: 'Failed to save produit' });
    });
});

// Route pour récupérer un produit par son ID
router.get('/produits/:id', (req, res) => {
  const produitId = req.params.id;

  Produit.findById(produitId)
    .then((produit) => {
      if (!produit) {
        return res.status(404).json({ error: 'Produit not found' });
      }

      res.json(produit);
    })
    .catch((error) => {
      console.error('Error fetching produit:', error);
      res.status(500).json({ error: 'Failed to fetch produit' });
    });
});

// Route pour mettre à jour un produit par son ID
router.put('/produits/:id', (req, res) => {
  const produitId = req.params.id;
  const { name, description, image, price } = req.body;

  Produit.findByIdAndUpdate(produitId, { name, description, image, price }, { new: true })
    .then((updatedProduit) => {
      if (!updatedProduit) {
        return res.status(404).json({ error: 'Produit not found' });
      }

      res.json(updatedProduit);
    })
    .catch((error) => {
      console.error('Error updating produit:', error);
      res.status(500).json({ error: 'Failed to update produit' });
    });
});

// Route pour supprimer un produit par son ID
router.delete('/produits/:id', (req, res) => {
  const produitId = req.params.id;

  Produit.findByIdAndDelete(produitId)
    .then((deletedProduit) => {
      if (!deletedProduit) {
        return res.status(404).json({ error: 'Produit not found' });
      }

      res.json(deletedProduit);
    })
    .catch((error) => {
      console.error('Error deleting produit:', error);
      res.status(500).json({ error: 'Failed to delete produit' });
    });
});

module.exports = router;
