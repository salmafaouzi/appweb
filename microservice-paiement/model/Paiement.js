const mongoose = require('mongoose');

// Define the Paiement Schema
const paiementSchema = new mongoose.Schema({
  idCommande: {
    type: Number,
    unique: true
  },
  montant: Number,
  numeroCarte: Number
});

// Create the Paiement model
const Paiement = mongoose.model('Paiement', paiementSchema);

module.exports = Paiement;
