const express = require('express');
const router = express.Router();
const Paiement = require('../../model/Paiement');
const PaiementExistantException = require('../../exceptions/PaiementExistantException');
const PaiementImpossibleException  = require('../../exceptions/PaiementImpossibleException');


// Opération pour enregistrer un paiement
router.post('/', async (req, res) => {
  try {
    const paiementData = req.body;

    // Vérifions s'il y a déjà un paiement enregistré pour cette commande
    const paiementExistant = await Paiement.findOne({ idCommande: paiementData.idCommande });
    if (paiementExistant) {
      throw new PaiementExistantException("Cette commande est déjà payée");
    }

    // Enregistrer le paiement
    const nouveauPaiement = new Paiement(paiementData);
    const savedPaiement = await nouveauPaiement.save();

    // On renvoie 201 CREATED pour notifier le client que le paiement a été enregistré
    res.status(201).json(savedPaiement);
  } catch (error) {
    // Error occurred while processing the paiement
    console.error('Error during paiement:', error);
    if (error instanceof PaiementExistantException || error instanceof PaiementImpossibleException) {
      res.status(400).json({ error: error.message });
    } else {
      res.status(500).json({ error: 'Paiement impossible' });
    }
  }
});

module.exports = router;
