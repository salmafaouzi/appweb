const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
require('dotenv').config();

// Import the paiements routes
const paiementRoutes = require('./routes/api/paiements');

// Create an Express app
const app = express();


app.use(bodyParser.json());

// Connect to MongoDB
mongoose.connect(process.env.MONGODB_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
  .then(() => {
    console.log('Connected to MongoDB');
  })
  .catch((error) => {
    console.error('Error connecting to MongoDB:', error);
  });


// Use the paiements routes
app.use('/api', paiementRoutes);

// Start the server
const port = 6000;
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
