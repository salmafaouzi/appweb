import React, { useEffect, useState } from 'react';

const App = () => {
  const [items, setItems] = useState([]);

  useEffect(() => {
    fetch('/api/items') // Mettez à jour l'URL pour correspondre à votre route pour récupérer les items
      .then((response) => response.json())
      .then((data) => {
        setItems(data);
      })
      .catch((error) => {
        console.error('Error retrieving items:', error);
      });
  }, []);

  return (
    <div className="container">
      <h1 className="text-center">Application Mcommerce</h1>
      <div className="row">
        {items.map((item) => (
          <div key={item._id} className="col-md-4 my-1">
            <a href={`/details-item/${item._id}`}>
              <img src={item.image} className="card-img-top" alt={item.titre} />
              <p>{item.titre}</p>
            </a>
          </div>
        ))}
      </div>
    </div>
  );
};

export default App;
