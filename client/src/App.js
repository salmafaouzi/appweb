import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import AccueilSpa from './components/AccueilSpa';
import FicheProduitSpa from './components/FicheProduitSpa';
import PaiementSpa from './components/PaiementSpa';
import PaiementStatus from './components/PaiementStatus';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

const App = () => {
  const [produits, setProduits] = useState([]);

  useEffect(() => {
    const baseUrl = 'http://localhost:3000';
    const apiProduitsUrl = new URL('/api/produits', baseUrl).toString();

    fetch(apiProduitsUrl)
      .then((response) => response.json())
      .then((data) => {
        setProduits(data);
      })
      .catch((error) => {
        console.error('Error retrieving produits:', error);
      });
  }, []);

  return (
    <Router>
      <div className="App">
        <Routes>
          <Route path="/" element={<AccueilSpa produits={produits} />} />
          <Route path="/details-produit/:id" element={<FicheProduitSpa produits={produits} />} />
          <Route path="/payer-commande/:id/:montant" element={<PaiementSpa />} /> 
          <Route path="/paiement-status/:id/:montant" element={<PaiementStatus />} />
        </Routes>
      </div>
    </Router>
  );
};

export default App;
