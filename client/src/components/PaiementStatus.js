import React from 'react';


const PaiementStatus = () => {
    const paiementOk = true;
  return (
    <div className="container">
      <h1 className="text-center">Application Mcommerce</h1>

      <div className="row">
        <div className="col-md-4 mx-auto mt-5 text-center">
          {paiementOk ? (
            <p>Paiement Accepté</p>
          ) : (
            <p>Le paiement n'a pas abouti</p>
          )}
        </div>
      </div>
    </div>
  );
};

export default PaiementStatus;
