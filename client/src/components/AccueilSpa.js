import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

const AccueilSpa = () => {
  const [produits, setProduits] = useState([]);

  useEffect(() => {
    const baseUrl = 'http://localhost:3000';
    const apiProduitsUrl = new URL('/api/produits', baseUrl).toString();

    fetch(apiProduitsUrl)
      .then((response) => response.json())
      .then((data) => {
        setProduits(data);
      })
      .catch((error) => {
        console.error('Error retrieving produits:', error);
      });
  }, []);

  return (
    <div className="container">
      <h1 className="text-center">Application Mcommerce</h1>
      <div className="row">
        {produits.map((produit) => (
          <div key={produit._id} className="col-md-4 my-1">
            <Link to={`/details-produit/${produit._id}`}>
              <img src={`http://localhost:8080${produit.image}`} className="card-img-top" alt={produit.name} />
              <p align="center">{produit.name} {produit.price}</p>
            </Link>
          </div>
        ))}
      </div>
    </div>
  );
};

export default AccueilSpa;
