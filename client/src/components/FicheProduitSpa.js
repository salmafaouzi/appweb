import React from 'react';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';


const FicheProduitSpa = ({ produits }) => {
  const { id } = useParams();

  // Find the produit with the matching ID
  const produit = produits.find((produit) => produit._id === id);

  // Handle case when produit is not found
  if (!produit) {
    return <div>Produit not found.</div>;
  }

  return (
    <div className="container">
      <h1 className="text-center">Application Mcommerce</h1>

      <div className="row">
        <div className="col-md-4 mx-auto mt-5 text-center">
          <img src={`http://localhost:8080${produit.image}`} className="card-img-top" alt="" />

          <p className="font-weight-bold">{produit.name}{produit.price}</p>

          <p>{produit.description}</p>

          <p>
            <Link to={`/payer-commande/${produit._id}/${produit.price}`} className="font-weight-bold">
              COMMANDER
            </Link>
          </p>
        </div>
      </div>
    </div>
  );
};

export default FicheProduitSpa;
