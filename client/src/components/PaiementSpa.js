import React from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { v4 as uuidv4 } from 'uuid'; 

const PaiementSpa = () => {
  const { price } = useParams();
  const navigate = useNavigate();
  const id = uuidv4();

  const handlePaiement = async () => {
    try {
      
      // Fetch the product details
      const productResponse = await fetch(`/api/produits/${id}`);
      if (!productResponse.ok) {
        // Product not found
        throw new Error('Product not found');
      }
      const productData = await productResponse.json();
  
      // Prepare the data for the commande
      const commandeData = {
        produitId: productData._id, // Use _id as the property name
        dateCommande: new Date(),
        quantite: 1,
        commandePayee: true,
      };
  
      // Make a POST request to the API to create the commande
      const commandeResponse = await fetch('/api/commandes', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(commandeData),
      });
  
      if (commandeResponse.ok) {
        // Commande saved successfully
        navigate(`/paiement-status/${id}/${price}`, { state: { paiementOk: true } });
      } else {
        // Commande creation failed
        navigate(`/paiement-status/${id}/${price}`, { state: { paiementOk: false } });
      }
    } catch (error) {
      // Error occurred while saving the commande or fetching the product details
      console.error('Error saving commande:', error);
      navigate(`/paiement-status/${id}/${price}`, { state: { paiementOk: false } });
    }
  };
  

  
  return (
    <div className="container">
      <h1 className="text-center">Application Mcommerce</h1>

      <div className="row">
        <div className="col-md-4 mx-auto mt-5 text-center">
          <p>
            Ici l'utilisateur sélectionne en temps normal un moyen de paiement et entre les informations de sa carte bancaire.
            <br />
            Nous allons éviter d'ajouter les formulaires nécessaires afin de garder l'application la plus basique et simple possible pour la suite.
            <br />
            Si vous vous sentez à l'aise, vous pouvez créer un formulaire pour accepter le numéro de la CB, que vous traiterez dans le contrôleur grâce à un <i>PostMapping</i>.
          </p>

          {/* On passe l'id de la commande ET le montant de celle-ci afin que le contrôleur ait les informations nécessaires pour faire appel au microservice de paiement */}
          <p>
            <button onClick={handlePaiement} className="font-weight-bold">Payer Ma Commande</button>
          </p>
          <p></p>
        </div>
      </div>
    </div>
  );
};

export default PaiementSpa;
